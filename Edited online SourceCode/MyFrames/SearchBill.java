
package MyFrames;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;


public class SearchBill extends javax.swing.JInternalFrame {

    /**
     * Creates new form SearchBill
     */
    public SearchBill() {
        initComponents();
        this.setLocation(400,45);
    }
    public Connection getConnection()
    {
        Connection con;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/qb","root","password");
            return con;
        } catch(Exception e)
        {
           return null;
        }
    }
    
    public ArrayList<BillSearch> List(String ValToSearch)
    {
        ArrayList<BillSearch> bsearch = new ArrayList<BillSearch>();
        Statement st;
        ResultSet rs;
        try{
            Connection con = getConnection();
            st=con.createStatement();
             String query="SELECT * FROM `bill` WHERE `bno`="+jTextField1.getText()+";";
            rs=st.executeQuery(query);
            
            BillSearch billsearch;
            
            while(rs.next())
            {
               billsearch = new BillSearch(
                                rs.getInt("bno"), rs.getString("cname"),rs.getInt("amt"),rs.getString("pdate")
               ); 
               bsearch.add(billsearch);
            }
            
            
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return bsearch;
    
    }
    
        public void findbill()
        {
            ArrayList <BillSearch> bills = List(jTextField1.getText());
            DefaultTableModel model = new DefaultTableModel();
            model.setColumnIdentifiers(new Object[]{"Billno","Customer Name","Amount","Date"});
            Object[] row =new Object [4] ;
            for(int i=0;i < bills.size(); i++)
            {
                row[0] = bills.get(i).getbno();
                row[1] = bills.get(i).getcname();
                row[2] = bills.get(i).getamt();
                row[3] = bills.get(i).pdate();
                model.addRow(row);
            }
            jTable1.setModel(model);
            
        }
   
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Search Bill");
        setToolTipText("");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Search bill");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel2.setText("Bill No");

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 102));
        jButton1.setIcon(new javax.swing.ImageIcon("C:\\Users\\SAMEER\\Desktop\\search-32.png")); // NOI18N
        jButton1.setText("Search");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bill No", "Customer Name", "Total Amount", "Date"
            }
        ));
 
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
      findbill();  // TODO add your handling code here:
    }

        private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1; 
    private javax.swing.JTextField jTextField1;
    }
