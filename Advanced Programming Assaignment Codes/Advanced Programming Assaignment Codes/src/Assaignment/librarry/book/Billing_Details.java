package Assaignment.librarry.book;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.protocol.Resultset;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Billing_Details extends JFrame {

	private JPanel contentPane;
	private JTextField Price_field;
	private JTextField textField_1;
	private JComboBox product_combobox;
private	JLabel no_of_products ;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Billing_Details frame = new Billing_Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	private void productDropdown() {
		try {
				String product = "select * from AddProducts";
				Statement statement =  connection.createStatement();
				ResultSet sResultset = statement.executeQuery(product);
				while(sResultset.next()) {
					product_combobox.addItem(sResultset.getString("product_name"));
					
					
				}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public Billing_Details() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("com.mysql.cj.jdbc.Driver", "root", "root");
			JOptionPane.showMessageDialog(null, " Connection SUCCESSFUL");
		} catch (Exception e) {
			// TODO: handle exception

		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(350, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		product_combobox = new JComboBox();
		product_combobox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String fetch_row = "select * from AddProducts where product_name =? ";
					PreparedStatement statement = connection.prepareStatement(fetch_row);
					statement.setString(1, (String)product_combobox.getSelectedItem());
							ResultSet set =statement.executeQuery();
							while (set.next()) {
								no_of_products.setText(set.getString("NofUni"));
								Price_field.setText(set.getString("Product_Price"));
							
							}
					
				} catch (Exception e2) {
					// TODO: handle exception
				}
				
				
				
				
			}
		});
		product_combobox.setBounds(10, 76, 183, 20);
		contentPane.add(product_combobox);

		no_of_products = new JLabel("");
		no_of_products.setBounds(226, 82, 46, 14);
		contentPane.add(no_of_products);

		Price_field = new JTextField();
		Price_field.setBounds(317, 76, 95, 20);
		contentPane.add(Price_field);
		Price_field.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(440, 76, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(556, 75, 89, 23);
		btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(btnSubmit);

		JLabel lblSelectBook = new JLabel("Select Book");
		lblSelectBook.setBounds(24, 51, 140, 14);
		lblSelectBook.setFont(new Font("Tahoma", Font.BOLD, 13));
		contentPane.add(lblSelectBook);

		JLabel lblUnitsOfLeft = new JLabel("Units of left");
		lblUnitsOfLeft.setBounds(203, 52, 95, 14);
		lblUnitsOfLeft.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(lblUnitsOfLeft);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(341, 51, 71, 14);
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(lblPrice);

		JLabel lblDiscount = new JLabel("quantity");
		lblDiscount.setBounds(439, 51, 71, 14);
		lblDiscount.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(lblDiscount);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 134, 577, 361);
		contentPane.add(scrollPane);
		productDropdown();
	}
}
