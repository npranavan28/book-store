package Assaignment.librarry.book;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddProducts extends JFrame {

	private JPanel contentPane;
	private JTextField product_name;
	private JTextField product_price;
	private JTextField nof_field;
	private JTextField tax_field;
	private JTextField Product_description;
	Connection connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddProducts frame = new AddProducts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void CreateTableNew() {
		try {
			java.sql.DatabaseMetaData data = connection.getMetaData();
			ResultSet resultset = data.getTables(null, null, "AddProducts", null);
			if (resultset.next()) {

			} else {
				String create_table = "Create table AddProducts(" + "Product_Name varchar(20)," + "Product_Price int,"
						+ "NofUni int," + "Tax int," + "Description varchar(45))";
				PreparedStatement pStatement = connection.prepareStatement(create_table);
				pStatement.executeUpdate();
				JOptionPane.showMessageDialog(null, "Table create_success");

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	/**
	 * Create the frame.
	 */

	public AddProducts() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("com.mysql.cj.jdbc.Driver", "root", "root");
			JOptionPane.showMessageDialog(null, " Connection SUCCESSFUL");
		} catch (Exception e) {
			// TODO: handle exception

		}

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAddProducts = new JLabel("Add Products");
		lblAddProducts.setFont(new Font("Felix Titling", Font.BOLD, 30));
		lblAddProducts.setBounds(194, 11, 300, 51);
		contentPane.add(lblAddProducts);

		JLabel lblProductname = new JLabel("Product_Name");
		lblProductname.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblProductname.setBounds(53, 131, 123, 25);
		contentPane.add(lblProductname);

		product_name = new JTextField();
		product_name.setBounds(171, 134, 145, 20);
		contentPane.add(product_name);
		product_name.setColumns(10);

		JLabel lblNewLabel = new JLabel("Product_Price");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(53, 167, 114, 25);
		contentPane.add(lblNewLabel);

		product_price = new JTextField();
		product_price.setBounds(171, 167, 145, 20);
		contentPane.add(product_price);
		product_price.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("No_of_units");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_1.setBounds(53, 204, 114, 25);
		contentPane.add(lblNewLabel_1);

		nof_field = new JTextField();
		nof_field.setBounds(171, 207, 145, 20);
		contentPane.add(nof_field);
		nof_field.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Tax");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setBounds(53, 240, 81, 25);
		contentPane.add(lblNewLabel_2);

		tax_field = new JTextField();
		tax_field.setBounds(171, 243, 145, 20);
		contentPane.add(tax_field);
		tax_field.setColumns(10);

		Product_description = new JTextField();
		Product_description.setBounds(181, 284, 145, 51);
		contentPane.add(Product_description);
		Product_description.setColumns(10);

		JLabel lblProductdescription = new JLabel("Product_Description");
		lblProductdescription.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblProductdescription.setBounds(47, 292, 129, 32);
		contentPane.add(lblProductdescription);

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String insert_to_products = "insert into AddProducts values(?,?,?,?,?)";
				try {
					PreparedStatement statement = connection.prepareStatement(insert_to_products);
					statement.setString(1, product_name.getText());
					statement.setString(2, product_price.getText());
					statement.setString(3, nof_field.getText());
					statement.setString(4, tax_field.getText());
					statement.setString(5, Product_description.getText());
					int data_inserted = statement.executeUpdate();
					
					if (data_inserted>0) {
						
						JOptionPane.showMessageDialog(null, "data inserted successfully");
						product_name.setText("");
						product_price.setText("");
						nof_field.setText("");
						tax_field.setText("");
						Product_description.setText("");
						
					} else {
						JOptionPane.showMessageDialog(null, "data inserted not correctly");

					}
					
					
				} catch (Exception e2) {
					// TODO: handle exception
				}

			}
		});
		btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSubmit.setBounds(184, 394, 89, 23);
		contentPane.add(btnSubmit);
		CreateTableNew();
	}
}
