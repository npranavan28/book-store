package Assaignment.librarry.book;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LibrarianView extends  javax.swing.JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LibrarianView frame = new LibrarianView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LibrarianView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("history");
		btnNewButton.setBounds(34, 42, 121, 84);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("transcation");
		btnNewButton_1.setBounds(213, 39, 186, 90);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("AddProducts");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AddProducts().setVisible(true);
				
			
			
			}
		});
		btnNewButton_2.setBounds(158, 160, 154, 90);
		contentPane.add(btnNewButton_2);
	}

}
